;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Personal
;; -------------------------
(setq user-full-name "Richard Gagen"
      user-mail-address "richard@gagen.io")

;; OS setup
;; -------------------------
(when (eq system-type 'darwin)
  ;; Option key setup
  (setq mac-command-modifier 'none
        mac-right-option-modifier 'none
        mac-option-modifier 'meta))

;; General Emacs Defaults
;; --------------------------
(setq-default fill-column 80                 ; Character limit for fill-column
              reb-re-syntax 'rx              ; Use rx system for regex builder
              delete-by-moving-to-trash t    ; Move deleted files to trash
              window-combination-resize t    ; Take new window space from all
              major-mode 'org-mode)          ; New buffers in org mode (not fundamental)

(setq undo-limit 80000000                    ; Raise undo-limit to 80Mb
      evil-want-file-undo t                  ; Graunular undo
      auto-save-default t                    ; Switch on autosave
      dired-dwim-target t                    ; In dired target other open windows
      display-line-numbers-type 'relative)   ; Relative line numbers

(display-time-mode 1)                        ; Display time in mode-bar
(global-subword-mode 1)                      ; Allow stepping through CamlCase

;; Move changes made via the customisation interface to a specific file
(setq-default custom-file (expand-file-name "custom.el" doom-private-dir))
(when (file-exists-p custom-file)
  (load custom-file))

;; Appearance
;; ----------------------

(setq doom-theme 'doom-vibrant)              ; Set default theme
(delq! t custom-theme-load-path)             ; Remove standard themes

(defun rg-modeline-conditional-buffer-encoding ()
  "Only display encoding on modeline when not UTF-8"
  (setq-local doom-modeline-buffer-encoding
              (unless (or (eq buffer-file-coding-system 'utf-8-unix)
                          (eq buffer-file-coding-system 'utf-8)))))

(add-hook 'after-change-major-mode-hook
          #'rg-modeline-conditional-buffer-encoding)

;; Windows
;; ----------------------

;; Set windows to switch to on splitting
(setq evil-vsplit-window-right t
      evil-split-window-below t)

;; Ask which buffer to load after splitting window
(defadvice! prompt-for-buffer (&rest _)
  :after '(evil-window-split evil-window-vsplit)
  (+ivy/switch-buffer))

;; Projectile
;; ----------------------
(setq projectile-project-search-path '("~/Projects/"))

;; Dired
;; ----------------------
(setq dired-dwim-target t)

;; File mode hooks
;; --------------------------
(add-to-list 'auto-mode-alist '("\\.sh\\'" . sh-mode))

;; Persp-mode
;; -------------------------

;; Inhibit new workspace on emacsclient
(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))

;; Flyspell
(map! :leader :desc "Correct spelling" "§" #'flyspell-correct-word-before-point)

;; Requires
;; ----------------------
;; TODO switch to load! and add-load-path! macros

(add-to-list 'load-path "~/.doom.d/modules/")
(require 'rg-ess)
(require 'rg-latex)
(require 'rg-text)

;;; config.el ends here
