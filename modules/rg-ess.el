;;; ess.el -*- lexical-binding: t; -*-

;; Keybindings
;; -------------------------------------------
(map! (:when (featurep! :lang ess)
        (:map comint-mode-map
          :localleader
          :desc "kill-input" "k" #'comint-kill-input
          :desc "beginning-input" "a" #'comint-bol
          :desc "maximum-output" "o" #'comint-show-maximum-output
          :desc "delete-output" "d" #'comint-delete-output
          :desc "copy-old-input" "RET" #'comint-copy-old-input
          :desc "search-forwards" "/" #'comint-forward-matching-input
          :desc "search-backwards" "?" #'comint-backward-matching-input
          :desc "clean-transcript" "c" #'ess-transcript-clean-region)))

;;; Provide
;;; -----------------------------------------
(provide 'rg-ess)
