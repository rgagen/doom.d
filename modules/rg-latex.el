;;; rg-latex.el -*- lexical-binding: t; -*-
;;; LaTeX/AucTeX setup

;;; General
(setq reftex-default-bibliography '("~/OneDrive/Documents/References/all_references.bib"))

;;; Hooks
;;; ---------------------------------------------
(add-hook! 'LaTeX-mode-hook  #'rg-reftex-vars
                             #'rg-latex-keys)

(defun rg-reftex-vars ()
  (setq reftex-enable-partial-scans t
        reftex-save-parse-info t
        reftex-use-multiple-selection-buffers t
        reftex-plug-into-AUCTeX t))

;;; Key Bindings
;;; ---------------------------------------------
(defun rg-latex-keys ()
  (map! (:when (featurep! :lang latex)
          (:map LaTeX-mode-map
            :localleader
            :desc "Insert Character" "-" #'insert-char
            :desc "Comment paragraph" "%" #'TeX-comment-or-uncomment-paragraph
            :desc "Acronym" "a" #'rg-acronym
            :desc "Run biber" "b" #'TeX-run-Biber
            :desc "Compile" "C" #'TeX-command-run-all
            :desc "Citation" "c" #'reftex-citation
            :desc "Insert environment" "e" #'LaTeX-environment
            :desc "Home" "h" #'TeX-home-buffer
            :desc "Insert item" "i" #'LaTeX-insert-item
            :desc "Macro" "m" #'TeX-insert-macro
            :desc "Cross-reference" "r" #'reftex-reference
            :desc "Add Aspell word" "S" #'rg-append-aspell-current
            :desc "Section" "s" #'LaTeX-section
            :desc "View" "v" #'TeX-view
            :desc "Close env" "x" #'LaTeX-close-environment
            :desc "Math mode" "~" #'LaTeX-math-mode))))

;; Functions
;;; ---------------------------------------------
(defun rg-latex-fullstop-infront-of-citation()
  "Moves latex citations at the end of sentences to after the fullstop"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
            (rx "\\" (group (optional
                             (or "paren" "auto" "text"))
                            "cite")
                "{" (group (+ alnum)) "}.")
            nil t)
      (replace-match (concat ". \\\\" (match-string 1)
                             "{" (match-string 2)
                             "}")))))

(defun rg-latex-fullstop-behind-citation()
  "Moves latex citations at the end of sentences to before the fullstop"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
            (rx ". \\" (group (optional
                               (or "paren" "auto" "text"))
                              "cite")
                "{" (group (+ alnum)) "}")

            nil t)
      (replace-match (concat " \\\\" (match-string 2)
                             "{" (match-string 3)
                             "}.")))))

(defun rg-acronym ()
  "Make the current word a latex acronym."
  (interactive)
  (let* ((bounds (if (use-region-p)
                     (cons (region-beginning) (region-end))
                   (bounds-of-thing-at-point 'symbol)))
         (text (buffer-substring-no-properties (car bounds) (cdr bounds))))
    (when bounds
      (delete-region (car bounds) (cdr bounds))
      (insert (concat "\\ac{" text "}")))))

;;; Provide
;;; ---------------------------------------------
(provide 'rg-latex)
