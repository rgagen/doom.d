;;; rg-text.el -*- lexical-binding: t; -*-

;;; Variables
;;; -------------------------------------------
(setf sentence-end-double-space nil)
(setq ispell-dictionary "en")

;;; Hooks
;;; -------------------------------------------
(add-hook! 'org-mode-hook 'rg-org-vars
                          'reftex-mode)
(add-hook 'markdown-mode-hook 'reftex-mode)

;; Org Mode
(defun rg-org-vars ()
  ;; Org Reg
  (setq org-ref-bibliography-notes "~/OneDrive/Documents/Bibliography/notes.org"
        org-ref-default-bibliography '("~/OneDrive/Documents/Bibliography/all_references.bib")
        org-ref-pdf-directory "~/OneDrive/Documents/References/")

  ;; Org-Ref open pdf with system pdf viewer (works on mac)
  (setq bibtex-completion-pdf-open-function
        (lambda (fpath)
          (start-process "open" "*open*" "open" fpath))))


;;; Key Bindings
;;; ---------------------------------------------
(map! (:when (featurep! :lang org)
       (:map org-mode-map
        :localleader
        :desc "Org-Ref Citation" "C" #'org-ref-insert-cite-with-completion)))

(map! (:when (featurep! :lang markdown)
       (:map markdown-mode-map
        :localleader
        :desc "reftex citation" "c" #'rg-markdown-reftex-citation)))

;;; Functions
;;; -------------------------------------------
(defun rg-text-title-caps (beg end)
  "English title capitalisation of marked region"
  (interactive "r")
  (save-excursion
    ;; List of words not to capitalise:
    (let ((do-not-capitalise '("a" "ago" "an" "and" "as"
                               "at" "but" "by" "for" "from"
                               "in" "into" "it" "next" "nor"
                               "of" "off" "on" "onto" "or"
                               "over" "past" "so" "the" "till"
                               "to" "up" "yet" "n" "t" "es" "s")))
      (goto-char beg) ; go to begin of first word:
      (capitalize-word 1)
      ;; go through the region, word by word:
      (while (< (point) end)
        (skip-syntax-forward "^w" end)
        (let ((word (thing-at-point 'word)))
          (if (stringp word)
              (if (member (downcase word) do-not-capitalise)
                  (downcase-word 1)
                (capitalize-word 1)))))
      ;; Always capitalise last word:
      (backward-word 1)
      (if (and (>= (point) beg)
               (not (member (or (thing-at-point 'word t) "s")
                            '("n" "t" "es" "s"))))
          (capitalize-word 1)))))

(defun rg-markdown-reftex-citation ()
  (interactive)
  (let ((reftex-cite-format '((?\C-m . "[@%l]")
                              (?p . "[@%l]")
                              (?t . "@%l")))
        (reftex-cite-key-separator "; @"))
    (reftex-citation)))

(defun rg-clean-dictonary ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (forward-line 1)                    ; Do not remove header
    (narrow-to-region (point) (point-max))
    (flush-lines "[^A-Za-z'\n\r]")      ; Non-ASCII letters (minus new-line and apostrophes)
    (flush-lines "^[^a-zA-Z]")          ; Non-letter start
    (flush-lines "[^a-zA-Z]$")          ; Non-letter end
    (widen)))

;;; Provide
;; ---------------------------------------------
(provide 'rg-text)
